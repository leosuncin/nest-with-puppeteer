import { PuppeteerModuleOptions } from './puppeteer-module-options.interface';

export interface PuppeteerModuleOptionsFactory {
  createPuppeteerOptions():
    | Promise<PuppeteerModuleOptions>
    | PuppeteerModuleOptions;
}
