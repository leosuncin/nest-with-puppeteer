import type { InjectionToken, ModuleMetadata, Type } from '@nestjs/common';

import { PuppeteerModuleOptions } from './puppeteer-module-options.interface';
import { PuppeteerModuleOptionsFactory } from './puppeteer-module-options-factory.interface';

export interface PuppeteerModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  /**
   * A unique name for the instance.
   *
   * If not specified, a default one will be used.
   *
   * @type {string}
   * @memberof PuppeteerModuleAsyncOptions
   */
  name?: string;
  /**
   * Reuse an injectable factory class created by another module.
   *
   * @type {Type<PuppeteerModuleOptionsFactory>}
   * @memberof PuppeteerModuleAsyncOptions
   */
  useExisting?: Type<PuppeteerModuleOptionsFactory>;
  /**
   * Use an injectable factory class to populate the module options,
   * such as launch options.
   *
   * @type {Type<PuppeteerModuleOptionsFactory>}
   * @memberof PuppeteerModuleAsyncOptions
   */
  useClass?: Type<PuppeteerModuleOptionsFactory>;
  /**
   * A factory function that will populate the module options,
   * such as launch options.
   *
   * @return {(Promise<PuppeteerModuleOptions>|PuppeteerModuleOptions)}
   * @memberof PuppeteerModuleAsyncOptions
   */
  useFactory?: (
    ...args: unknown[]
  ) => Promise<PuppeteerModuleOptions> | PuppeteerModuleOptions;
  /**
   * Inject any dependencies required by the Puppeteer module,
   * such as a configuration service that supplies the options and instance name.
   *
   * @type {Array<InjectionToken>}
   * @memberof PuppeteerModuleAsyncOptions
   */
  inject?: Array<InjectionToken>;
}
