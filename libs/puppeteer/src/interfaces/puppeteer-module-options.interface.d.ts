import { ConnectOptions, PuppeteerLaunchOptions } from 'puppeteer';

export interface PuppeteerModuleOptions {
  /**
   * The name of the instance of `PuppeteerModule`,
   * must be unique
   *
   * @type {string}
   * @memberof PuppeteerModuleOptions
   */
  name?: string;
  /**
   * The launch options for Puppeteer
   *
   * @type {PuppeteerLaunchOptions | ConnectOptions}
   * @memberof PuppeteerModuleOptions
   * @see https://pptr.dev/api/puppeteer.puppeteernodelaunchoptions
   * @see https://pptr.dev/api/puppeteer.connectoptions
   */
  launchOptions?: PuppeteerLaunchOptions | ConnectOptions;
}
