import type { InjectionToken } from '@nestjs/common';

export const PUPPETEER_MODULE_OPTIONS: InjectionToken = Symbol.for(
  'PuppeteerModuleOptions',
);
