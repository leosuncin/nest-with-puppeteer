import { DynamicModule, Module } from '@nestjs/common';

import { PuppeteerModuleAsyncOptions } from './interfaces/puppeteer-module-async-options.interface';
import { PuppeteerModuleOptions } from './interfaces/puppeteer-module-options.interface';
import { createPuppeteerProviders } from './puppeteer.provider';
import { PuppeteerCoreModule } from './puppeteer-core.module';

@Module({})
export class PuppeteerModule {
  /**
   * Inject the PuppeteerModule synchronously.
   *
   * @static
   * @param {PuppeteerModuleOptions['launchOptions']} [options] Options for the Browser to be launched
   * @param {String} [instanceName] A unique name for the instance.
   *
   *                                If not specified, a default name will be used.
   * @returns {DynamicModule}
   * @memberof PuppeteerModule
   */
  static forRoot(
    options?: PuppeteerModuleOptions['launchOptions'],
    instanceName?: string,
  ): DynamicModule {
    return {
      module: PuppeteerModule,
      imports: [PuppeteerCoreModule.forRoot(options, instanceName)],
    };
  }
  /**
   * Inject the PuppeteerModule asynchronously, allowing any dependencies
   * such as a configuration service to be injected first.
   *
   * @static
   * @param {PuppeteerModuleAsyncOptions} options Options for asynchronous injection
   * @returns {DynamicModule}
   * @memberof PuppeteerModule
   */
  static forRootAsync(options: PuppeteerModuleAsyncOptions): DynamicModule {
    return {
      module: PuppeteerModule,
      imports: [PuppeteerCoreModule.forRootAsync(options)],
    };
  }
  /**
   * Inject pages.
   *
   * @static
   * @param {Array<string>} [pages] An array with the names of the pages to be injected.
   * @param {string} [instanceName] A unique name for the instance.
   *
   *                                If not specified, a default name will be used.
   * @returns {DynamicModule}
   * @memberof PuppeteerModule
   */
  static forFeature(
    pages?: Array<string>,
    instanceName?: string,
  ): DynamicModule {
    const providers = createPuppeteerProviders(pages, instanceName);

    return {
      module: PuppeteerModule,
      providers,
      exports: providers,
    };
  }
}
