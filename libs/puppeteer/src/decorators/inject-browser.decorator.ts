import { Inject } from '@nestjs/common';

import { getBrowserToken } from '../utils/get-browser.token';

/**
 * Inject the [Browser](https://pptr.dev/api/puppeteer.browser) object associated with the instance of Puppeteer
 *
 * @param name The unique name associated with the browser
 */
export const InjectBrowser = (name?: string) => Inject(getBrowserToken(name));
