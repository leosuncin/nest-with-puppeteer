import { Inject } from '@nestjs/common';

import { getPageToken } from '../utils/get-page.token';

/**
 * Inject the [Page](https://pptr.dev/api/puppeteer.page) object associated with the instance of Puppeteer
 *
 * @param name The unique name associated with the page
 */
export const InjectPage = (name?: string) => Inject(getPageToken(name));
