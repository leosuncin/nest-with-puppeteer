import { Inject } from '@nestjs/common';

import { getBrowserContextToken } from '../utils/get-browser-context.token';

/**
 * Inject the [BrowserContext](https://pptr.dev/api/puppeteer.browsercontext) object associated with the instance of Puppeteer
 *
 * @param name The unique name associated with the browser context
 */
export const InjectContextBrowser = (name?: string) =>
  Inject(getBrowserContextToken(name));
