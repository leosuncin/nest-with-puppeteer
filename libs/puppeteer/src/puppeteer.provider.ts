import type { FactoryProvider, Provider } from '@nestjs/common';
import type { BrowserContext, Page } from 'puppeteer';

import { DEFAULT_CHROME_LAUNCH_OPTIONS } from './constants/default-chrome-launch-options.constant';
import { PuppeteerModuleAsyncOptions } from './interfaces/puppeteer-module-async-options.interface';
import { PuppeteerModuleOptions } from './interfaces/puppeteer-module-options.interface';
import { PuppeteerModuleOptionsFactory } from './interfaces/puppeteer-module-options-factory.interface';
import { PUPPETEER_MODULE_OPTIONS } from './tokens/module-options.symbol';
import { getBrowserContextToken } from './utils/get-browser-context.token';
import { getPageToken } from './utils/get-page.token';

function createAsyncOptionsProvider({
  inject = [],
  useClass,
  useExisting,
  useFactory,
}: PuppeteerModuleAsyncOptions): FactoryProvider<PuppeteerModuleOptions> {
  if (useFactory) {
    return {
      provide: PUPPETEER_MODULE_OPTIONS,
      useFactory,
      inject,
    };
  }

  if (useExisting) {
    return {
      provide: PUPPETEER_MODULE_OPTIONS,
      useFactory: (optionsFactory: PuppeteerModuleOptionsFactory) =>
        optionsFactory.createPuppeteerOptions(),
      inject: [useExisting],
    };
  }

  if (useClass) {
    return {
      provide: PUPPETEER_MODULE_OPTIONS,
      useFactory: (optionsFactory: PuppeteerModuleOptionsFactory) =>
        optionsFactory.createPuppeteerOptions(),
      inject: [useClass],
    };
  }

  throw new Error('Invalid PuppeteerModule options');
}

/**
 * Resolve a provider for the launch options
 *
 * @private
 * @param {PuppeteerModuleAsyncOptions} options
 * @returns {Array<Provider<PuppeteerModuleOptions>>}
 */
export function createAsyncProviders(
  options: PuppeteerModuleAsyncOptions,
): Array<Provider> {
  if (options.useExisting || options.useFactory) {
    return [createAsyncOptionsProvider(options)];
  }

  if (options.useClass) {
    return [
      createAsyncOptionsProvider(options),
      {
        provide: options.useClass,
        useClass: options.useClass,
      },
    ];
  }

  return [
    {
      provide: PUPPETEER_MODULE_OPTIONS,
      useValue: DEFAULT_CHROME_LAUNCH_OPTIONS,
    },
  ];
}

/**
 * Creates a providers for pages
 *
 * @private
 * @param {string[]} [pages=[]]
 * @param {string} [instanceName]
 * @returns {Array<Provider<Page>>}
 */
export function createPuppeteerProviders(
  pages: string[] = [],
  instanceName?: string,
): Array<Provider<Page>> {
  return pages.map((page) => ({
    provide: getPageToken(page),
    inject: [getBrowserContextToken(instanceName)],
    useFactory: (context: BrowserContext) => context.newPage(),
  }));
}
