import {
  DynamicModule,
  FactoryProvider,
  Global,
  Inject,
  Module,
  OnApplicationShutdown,
  OnModuleDestroy,
  ValueProvider,
} from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import type {
  Browser,
  BrowserContext,
  ConnectOptions,
  Page,
  PuppeteerLaunchOptions,
} from 'puppeteer';
import { launch, connect } from 'puppeteer';

import { DEFAULT_CHROME_LAUNCH_OPTIONS } from './constants/default-chrome-launch-options.constant';
import { DEFAULT_PUPPETEER_INSTANCE_NAME } from './constants/default-instance-name.constant';
import { PuppeteerModuleAsyncOptions } from './interfaces/puppeteer-module-async-options.interface';
import { PuppeteerModuleOptions } from './interfaces/puppeteer-module-options.interface';
import { createAsyncProviders } from './puppeteer.provider';
import { PUPPETEER_INSTANCE_NAME } from './tokens/instance-name.symbol';
import { PUPPETEER_MODULE_OPTIONS } from './tokens/module-options.symbol';
import { getBrowserToken } from './utils/get-browser.token';
import { getBrowserContextToken } from './utils/get-browser-context.token';
import { getPageToken } from './utils/get-page.token';

@Global()
@Module({})
export class PuppeteerCoreModule
  implements OnApplicationShutdown, OnModuleDestroy
{
  constructor(
    @Inject(PUPPETEER_INSTANCE_NAME)
    private readonly instanceName: string,
    @Inject(ModuleRef)
    private readonly moduleRef: ModuleRef,
  ) {}

  static forRoot(
    launchOptions:
      | PuppeteerLaunchOptions
      | ConnectOptions = DEFAULT_CHROME_LAUNCH_OPTIONS,
    instanceName: string = DEFAULT_PUPPETEER_INSTANCE_NAME,
  ): DynamicModule {
    const instanceNameProvider: ValueProvider<string> = {
      provide: PUPPETEER_INSTANCE_NAME,
      useValue: instanceName,
    };
    const browserProvider: FactoryProvider<Browser> = {
      provide: getBrowserToken(instanceName),
      useFactory: () => {
        if ('browserWSEndpoint' in launchOptions) {
          return connect(launchOptions);
        }

        return launch(launchOptions);
      },
    };
    const browserContextProvider: FactoryProvider<BrowserContext> = {
      provide: getBrowserContextToken(instanceName),
      inject: [getBrowserToken(instanceName)],
      useFactory: (browser: Browser) => browser.defaultBrowserContext(),
    };
    const pageProvider: FactoryProvider<Page> = {
      provide: getPageToken(instanceName),
      inject: [getBrowserContextToken(instanceName)],
      useFactory: (context: BrowserContext) => context.newPage(),
    };

    return {
      module: PuppeteerCoreModule,
      providers: [
        instanceNameProvider,
        browserProvider,
        browserContextProvider,
        pageProvider,
      ],
      exports: [browserProvider, browserContextProvider, pageProvider],
    };
  }

  static forRootAsync(options: PuppeteerModuleAsyncOptions): DynamicModule {
    const instanceName = options.name ?? DEFAULT_PUPPETEER_INSTANCE_NAME;
    const instanceNameProvider: ValueProvider<string> = {
      provide: PUPPETEER_INSTANCE_NAME,
      useValue: instanceName,
    };
    const browserProvider = {
      provide: getBrowserToken(instanceName),
      inject: [PUPPETEER_MODULE_OPTIONS],
      useFactory(puppeteerModuleOptions: PuppeteerModuleOptions) {
        if (
          puppeteerModuleOptions.launchOptions &&
          'browserWSEndpoint' in puppeteerModuleOptions.launchOptions
        ) {
          return connect(puppeteerModuleOptions.launchOptions);
        }

        return launch(
          puppeteerModuleOptions.launchOptions ?? DEFAULT_CHROME_LAUNCH_OPTIONS,
        );
      },
    };
    const browserContextProvider: FactoryProvider<BrowserContext> = {
      provide: getBrowserContextToken(instanceName),
      inject: [PUPPETEER_MODULE_OPTIONS, getBrowserToken(instanceName)],
      useFactory: (browser: Browser) => browser.createIncognitoBrowserContext(),
    };
    const pageProvider: FactoryProvider<Page> = {
      provide: getBrowserContextToken(instanceName),
      inject: [PUPPETEER_MODULE_OPTIONS, getPageToken(instanceName)],
      useFactory: (context: BrowserContext) => context.newPage(),
    };

    return {
      module: PuppeteerCoreModule,
      imports: options.imports,
      providers: [
        instanceNameProvider,
        ...createAsyncProviders(options),
        browserProvider,
        browserContextProvider,
        pageProvider,
      ],
      exports: [browserProvider, browserContextProvider, pageProvider],
    };
  }

  async onModuleDestroy() {
    const browser = this.moduleRef.get<Browser | undefined>(
      getBrowserToken(this.instanceName),
    );

    if (browser?.isConnected()) {
      await browser.disconnect();
    }
  }

  async onApplicationShutdown() {
    const browser = this.moduleRef.get<Browser | undefined>(
      getBrowserToken(this.instanceName),
    );

    try {
      await browser?.close();
    } catch {
      // ignore error
    }
  }
}
