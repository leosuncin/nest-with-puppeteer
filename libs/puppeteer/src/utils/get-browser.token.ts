import type { InjectionToken } from '@nestjs/common';

import { DEFAULT_PUPPETEER_INSTANCE_NAME } from '../constants/default-instance-name.constant';

/**
 * This function generates an injection token for the [Browser](https://pptr.dev/api/puppeteer.browser)
 *
 * @export
 * @param  {String} [instanceName] The unique name for the Puppeteer instance
 * @return {InjectionToken} The token to inject the browser
 */
export function getBrowserToken(instanceName?: string): InjectionToken {
  instanceName ??= DEFAULT_PUPPETEER_INSTANCE_NAME;

  return `${instanceName}Browser`;
}
