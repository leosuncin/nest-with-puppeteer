import type { InjectionToken } from '@nestjs/common';

import { DEFAULT_PUPPETEER_INSTANCE_NAME } from '../constants/default-instance-name.constant';

/**
 * This function generates an injection token for the [BrowserContext](https://pptr.dev/api/puppeteer.browsercontext)
 *
 * @export
 * @param  {String} [instanceName] The unique name for the Puppeteer instance
 * @return {InjectionToken} The token to inject the context
 */
export function getBrowserContextToken(instanceName?: string): InjectionToken {
  instanceName ??= DEFAULT_PUPPETEER_INSTANCE_NAME;

  return `${instanceName}Context`;
}
