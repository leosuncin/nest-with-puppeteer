import * as process from 'node:process';

import type { PuppeteerLaunchOptions } from 'puppeteer';

export const DEFAULT_CHROME_LAUNCH_OPTIONS: Readonly<PuppeteerLaunchOptions> = {
  headless: 'new',
  pipe: process.platform !== 'win32',
  ...(process.env.PUPPETEER_EXECUTABLE_PATH
    ? { executablePath: process.env.PUPPETEER_EXECUTABLE_PATH }
    : { channel: 'chrome' }),
  args: [
    // Enables TLS/SSL errors on localhost to be ignored (no interstitial, no blocking of requests).
    '--allow-insecure-localhost',
    // Allow non-secure origins to use the screen capture API and the desktopCapture extension API.
    '--allow-http-screen-capture',
    // https://codereview.chromium.org/2384163002
    '--no-zygote',
    // add --no-sandbox when running on Linux, required with --no-zygote
    typeof process.getuid === 'function' ? '--no-sandbox' : '',
  ],
};
