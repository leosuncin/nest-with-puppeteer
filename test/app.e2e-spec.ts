import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('/test (GET)', async () => {
    await request(app.getHttpServer())
      .get('/test')
      .query({ url: 'https://kitchen-sink.surge.sh' })
      .expect(HttpStatus.OK)
      .expect({
        heading: 'HTML5 Kitchen Sink',
      });
  });

  it('/pdf (GET)', async () => {
    await request(app.getHttpServer())
      .get('/pdf')
      .buffer()
      .query({ url: 'https://kitchen-sink.surge.sh' })
      .expect(HttpStatus.OK)
      .expect('Content-Type', 'application/pdf')
      .expect('Content-Disposition', 'attachment; filename=test.pdf');
  });
});
