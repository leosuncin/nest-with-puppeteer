import { Injectable, ServiceUnavailableException } from '@nestjs/common';
import { InjectPage } from '@app/puppeteer';
import { Page } from 'puppeteer';

@Injectable()
export class AppService {
  constructor(
    @InjectPage()
    private readonly page: Page,
  ) {}

  async getHeading(url: string): Promise<{ heading: string }> {
    await this.page.goto(url, { waitUntil: 'networkidle2' });

    const h1 = await this.page.$('[role="banner"] h1');

    if (!h1) {
      throw new ServiceUnavailableException('No heading element found');
    }

    const heading = await h1.evaluate((node) => {
      return node.textContent;
    });

    if (!heading) {
      throw new ServiceUnavailableException('No heading text found');
    }

    return { heading };
  }

  async getPdf(url: string) {
    await this.page.goto(url, { waitUntil: 'networkidle2' });

    return this.page.pdf();
  }
}
