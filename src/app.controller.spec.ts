import { Test, TestingModule } from '@nestjs/testing';
import { getPageToken } from '@app/puppeteer';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        {
          provide: getPageToken(),
          useValue: {
            goto: jest.fn().mockResolvedValue(undefined),
            $: jest.fn().mockResolvedValue({
              evaluate: jest.fn().mockResolvedValue('Hello World!'),
            }),
          },
        },
        AppService,
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', async () => {
      await expect(
        appController.getHeading('http://localhost'),
      ).resolves.toEqual({
        heading: 'Hello World!',
      });
    });
  });
});
