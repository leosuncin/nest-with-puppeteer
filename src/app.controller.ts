import { Readable } from 'node:stream';
import {
  BadRequestException,
  Controller,
  Get,
  Query,
  Res,
} from '@nestjs/common';
import type { Response } from 'express';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('test')
  getHeading(@Query('url') url: string) {
    if (!url) {
      throw new BadRequestException('url query param is required');
    }

    return this.appService.getHeading(url);
  }

  @Get('pdf')
  async getPdf(@Query('url') url: string, @Res() response: Response) {
    if (!url) {
      throw new BadRequestException('url query param is required');
    }

    const buffer = await this.appService.getPdf(url);
    const stream = new Readable();

    stream.push(buffer);
    stream.push(null);

    response.setHeader('Content-Type', 'application/pdf');
    response.setHeader('Content-Disposition', 'attachment; filename=test.pdf');
    response.setHeader('Content-Length', buffer.length);

    stream.pipe(response);
  }
}
