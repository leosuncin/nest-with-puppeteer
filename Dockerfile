# Install dependencies only when needed
FROM docker.io/node:lts-alpine as deps
ENV PUPPETEER_SKIP_DOWNLOAD=true
WORKDIR /var/src/app
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
COPY package*.json ./
RUN npm i --omit=dev

# Production image, copy all the files and run nest
FROM docker.io/node:lts-alpine as runner
ENV NODE_ENV production
ENV PORT 3000
ENV PUPPETEER_EXECUTABLE_PATH /usr/bin/chromium-browser
WORKDIR /var/src/app
RUN apk update && apk add --no-cache nmap dumb-init && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    apk update && \
    apk add --no-cache \
      chromium \
      harfbuzz \
      "freetype>2.8" \
      ttf-freefont \
      nss \
      udev
COPY --from=deps /var/src/app/node_modules ./node_modules
COPY --from=deps /var/src/app/package.json ./package.json
COPY dist .
RUN chown -R node:node .
USER node
EXPOSE 3000
CMD ["dumb-init", "node", "main.js"]
